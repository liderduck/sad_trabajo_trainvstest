# README #

Parte del trabajo final de SAD(sistemas de apoyo a la decisión). 

### ¿Que hace este repositorio? ###

* Este programa lee dos ficheros .arf.
* Uno de ellos lo escoge como entrenador y otro como test.
* Procesa el entrenador con diferentes algoritmos y filtros.
* Lo lanza contra el test para ver la tasa de acierto con los nuevos ajustes.
* Imprime por pantalla que ajuste tiene un mejor porcentaje de aciertos.

### ¿Como funciona este repositorio? ###

* Este programa lee un fichero .arff mediante path introducido por el usuario y lo asigna como conjunto de entrenamiento "train".
* Lee un fichero .arff mediante path introducido por el usuario y lo asigna como conjunto de test "test".
* Crea un clasificador de redes bayesianas (bayes net).
* Aplica varios algoritmos de busqueda(Hillclimber,K2...).
* Carga varios estimadores (BMAEstimator, SimpleEstimator...)
* Crea el evaluador.
* Evalúa el fichero TestVsTrain mediante Holdout.
* Imprime por pantalla cual es el mejor algoritmo.

### Tecnología ###

* Java
* API de Weka(http://www.cs.waikato.ac.nz/ml/weka/)

### Creador ###

* Jonathan Guijarro Garcia