package sad_trabajo_trainVsTest;




import weka.classifiers.Evaluation;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.net.estimate.BMAEstimator;
import weka.classifiers.bayes.net.estimate.SimpleEstimator;
import weka.classifiers.bayes.net.search.SearchAlgorithm;
import weka.classifiers.bayes.net.search.local.HillClimber;
import weka.classifiers.bayes.net.search.local.K2;
import weka.classifiers.bayes.net.search.local.SimulatedAnnealing;
import weka.classifiers.bayes.net.search.local.TAN;
import weka.core.Instances;


	public class Principal {
		
	    public static void main(String[] args) throws Exception {
	    	Lectura lect= new Lectura();	    	
						
	    	/////////////Lectura de datos/////////////
	    	Instances dataTrain;
	    	dataTrain = lect.cargarDatos(args[0]);
	    	
	    	Instances dataTest;
	    	dataTest = lect.cargarDatos(args[1]);
	    		    
	    	//creamos el clasificador para bayesNet
	    	BayesNet bayesNet = new BayesNet();
	    	//ahora los algortimos de busqueda
	    	HillClimber hill = new HillClimber();
	    	K2 k2 = new K2();
	    	SimulatedAnnealing simAnn = new SimulatedAnnealing();
	    	TAN tan = new TAN();
	    	SearchAlgorithm[] seralg = new SearchAlgorithm[4];
	    	seralg[0]=hill;
	    	seralg[1]=k2;
	    	seralg[2]=simAnn;
	    	seralg[3]=tan;
	    	
	    	//ahora con los estimadores
	    	
	    	BMAEstimator bma = new  BMAEstimator();
	    	SimpleEstimator simple =new SimpleEstimator();

	    	//creamos el evaluador
	    	Evaluation evaluador = new Evaluation(dataTest);
	    	
	    	double mejorFM=0;
	    	int k=0;
	    	int l=0;
	    	double actualFM;
	    	System.out.println("a evaluar");
	    	//cogemos los archivos para crear el holdout
			bayesNet.buildClassifier(dataTrain);
	    	
	    	for(int i=0;i<=2;i++){
	    		if (i==0){
	    			System.out.println("1");
	    			bayesNet.setEstimator(simple);
	    		}else if (i==2){
	    			System.out.println("2");
	    			bayesNet.setEstimator(bma);
	    		}
	    		for(int z=0;z<=seralg.length;z++){
	    			if(z==0){
	    				System.out.println("6");
	    				bayesNet.setSearchAlgorithm(k2);
	    			}else if(z==1){
	    				System.out.println("7");
	    				bayesNet.setSearchAlgorithm(hill);
	    			}else if(z==2){
	    				System.out.println("8");
	    				bayesNet.setSearchAlgorithm(simAnn);
	    			}else if((z==3)&& (i==0)){
	    				System.out.println("9");
	    				bayesNet.setSearchAlgorithm(tan);
	    			}
	    			
	    			System.out.println("          "+z);
	    			//creamos un evaluador
	    			evaluador.evaluateModel(bayesNet, dataTest);
	    			actualFM=evaluador.fMeasure(1);
	    			if( actualFM>mejorFM){
	    				mejorFM=actualFM;
	    				k=z;
	    				l=i;
	    			}
	    			
	    		}
	    	}
	    	
	    	if(l==0){
	    		System.out.println("el mejor estimador es simple");
	    	}else{
	    		System.out.println("el mejor estimador es bma");
	    	}
	    	
	    	if(k==0){
	    		System.out.println("el mejor algoritmo es k2");
	    	}else if(k==1){
	    		System.out.println("el mejor algoritmo es hill");
	    	}else if(k==2){
	    		System.out.println("el mejor algoritmo es simAnn");
	    	}else if(k==3){
	    		System.out.println("el mejor algoritmo es tan");
	    	}
	    }
}


